<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model {

	use RecordsActivity;

	protected $guarded = [];

	public static function boot() {
		parent::boot();
		static::addGlobalScope('replyCount', function ($builder) {
			$builder->withCount('replies');
		});

		static::deleting(function ($threads) {
			$threads->replies()->each(function ($reply) {
				$reply->activity()->delete();
			});

			$threads->replies()->delete();
			$threads->activity()->delete();
		});

	}

	public function path() {
		return '/threads/' . $this->channel->slug . '/' . $this->id;
	}

	public function replies() {
		return $this->hasMany(Reply::class)->orderBy('created_at', 'desc');
	}

	public function creator() {
		return $this->belongsTo(User::class, 'user_id');
	}

	public function channel() {
		return $this->belongsTo(Channel::class, 'channel_id');
	}

	public function addReply($reply) {
		$this->replies()->create($reply);
	}

}
