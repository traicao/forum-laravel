<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model {

	use RecordsActivity;

	protected $guarded = [];

	public static function boot() {
		parent::boot();

	}

	public function owner() {
		return $this->belongsTo(User::class, 'user_id');
	}

	public function thread() {
		return $this->belongsTo(Thread::class, 'thread_id');
	}

	public function favorites() {
		return $this->morphMany(Favorite::class, 'favorited');
	}

	public function favorite() {

		$attr = ['user_id' => auth()->id()];

		if (!$this->favorites()->where($attr)->exists()) {
			$this->favorites()->create($attr);
		}
	}

	public function isFavorited() {

		return $this->favorites()->where('user_id', auth()->id())->exists();
	}
}
