<?php
namespace App\Http\Controllers;
use App\User;

class ProfilesController extends Controller {
	/**
	 * Show the user's profile.
	 *
	 * @param  User $user
	 * @return \Response
	 */
	public function show(User $user) {

		$activities = $user->activity()->latest()->with('subject')->get()->groupBy(function ($activity) {
			return $activity->created_at->format('Y-m-d');
		});
		return view('profiles.show', [
			'profileUser' => $user,
			'activities' => $activities,
		]);
	}
}