@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>
                <img src="{{  asset('storage/images/'.$profileUser->avatar) }}"  class="img-circle" alt="image" width="50" height="50">
                <strong>{{ $profileUser->name }}</strong>
                <small class="text-muted time"> Since {{ $profileUser->created_at->diffForHumans() }}</small>
            </h1>
        </div>

        @foreach ($activities as $date => $activity)
            <div class="page-header time"> {{ $date }}</div>
            @foreach ($activity as $record)
                @include("profiles.activities.{$record->type}",['activity'=>$record])
            @endforeach
        @endforeach

       {{--  {{ $threads->links() }} --}}
    </div>
@endsection