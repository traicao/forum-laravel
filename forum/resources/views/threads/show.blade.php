@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8 ">

            <div class="panel panel-success">

                <div class="panel-heading">

                     <div class="level">

                         <h4 class="flex">

                             <img src="{{  asset('storage/images/'.$thread->creator->avatar) }}"  class="img-circle" alt="image" width="50" height="50">

                            <a href="{{ route('profile', $thread->creator) }}">
                                {{ $thread->creator->name }}
                            </a>

                            <small>posted : </small> {{$thread->title}}

                        </h4>

                        @if (Auth::check() && ($thread->creator->id == Auth::user()->id))

                            <div>

                            <form action="" method="POST" role="form">

                                {{ csrf_field() }}

                                {{ method_field('DELETE') }}

                                <button type="submit" method="POST" class="btn btn-danger">Delete</button>

                            </form>

                            </div>

                        @endif

                    </div>

                </div>

                <div class="panel-body">

                    {{$thread->body}}

                </div>

            </div>

             @foreach ($replies as $reply)

                 @include('threads.reply')

             @endforeach

             {{ $replies->links() }}

             @if (Auth::check())

                 <form method="POST" action={{ $thread->path() .'/replies' }}>

                    {{ csrf_field() }}

                    <div class="form-group">

                        <textarea name="body" id="body" class="form-control" placeholder="Have somthing to say?" rows="5">

                        </textarea>

                    </div>

                    <button type="submit" class="btn btn-default">Post</button>

                </form>

            @else

                <p class="text-center" >Please <a href={{ route('login') }}>sign in</a> to participate in this discussion</p>

            @endif

        </div>

            <div class="col-md-4">

                 <div class="panel panel-success">

                    <div class="panel-body">

                       <p>
                           This thread was published {{ $thread->created_at->diffForHumans() }} by

                           <a href="/profiles/{{ $thread->creator->name }}">{{ $thread->creator->name }}</a>
                           , and currently has {{ $thread->replies_count }} comments.

                       </p>

                    </div>

                </div>

            </div>

    </div>

</div>

@endsection
