@extends('layouts.app')

@section('content')

<div class="container">

        <div class="row content">

             <div class="col-sm-3 nav side-nav">
             <div class="btn-create">
                <a href="/threads/create">
                    <button type="button" class="btn btn-success btn-block">Create Thread</button>
                </a>
            </div>
            <hr>
            <div class="list-group">
                  @if (Auth::check())
                      <a href={{'/threads?by='.auth()->user()->name}} class="list-group-item borderless">My Thread</a>
                  @endif
                       <a href="/threads" class="list-group-item borderless">All Threads</a>
                       <a href="/threads?popular=1" class="list-group-item borderless">Popular Threads</a>
            </div>

            <hr>

            <div class="list-group">
                <a href="#" class="list-group-item disabled">Tag</a>
             @foreach ($channels as $channel)
                <a href="{{'/threads/'.$channel->slug}}" class="list-group-item borderless">{{$channel->slug}}</a>
            @endforeach
            </div>

             </div>
            <div class="col-sm-9">
                @foreach ($threads as $thread)

                <div class="panel panel-success">

                    <div class="panel-heading">

                         <div class="level">

                            <h4 class="flex">
                                <img src="{{  asset('storage/images/'.$thread->creator->avatar) }}"  class="img-circle" alt="Cinque Terre" width="50" height="50">
                                <a href="{{$thread->path()}}">{{ $thread->title}} </a>

                            </h4>

                            <div>
                                 <a href="{{$thread->path()}}">

                                    <button  type="button" class="btn btn-default btn-sm">

                                      <span class="glyphicon glyphicon-comment"></span>

                                      {{ $thread->replies_count }} replies

                                    </button>

                                 </a>
                            </div>

                         </div>

                    </div>

                    <div class="panel-body">

                        <div class="body">{{$thread->body}}</div>

                    </div>

                </div>

                @endforeach

                <div class="text-center">

                    @if(isset($popular))

                        {{$threads->appends(['popular' => $popular])->links()}}

                    @else

                        {{$threads->links()}}

                    @endif

                </div>

            </div>

        </div>

    {{-- <div class="row">

        <div class="col-md-8">

             @if(isset($popular))

                {{$threads->appends(['popular' => $popular])->links()}}

             @else

                {{$threads->links()}}

             @endif

        </div>
    </div> --}}

</div>
<hr>

<footer class="container text-center">
  <strong>Design by CrTrai</strong>
</footer>

@endsection
