<div class="panel panel-default">

    <div class="panel-heading">

     	<div class="level">

	     	<h5 class="flex">

				<img src="{{  asset('storage/images/'.$reply->owner->avatar) }}"  class="img-circle" alt="image" width="50" height="50">

		        <a href="{{ route('profile', $reply->owner) }}">

		            {{ $reply->owner->name }}

		    	</a>

		    	 said {{$reply->created_at->diffForHumans()}}

		    </h5>

			<div >

				<form method="POST" action="/replies/{{$reply->id}}/favorites" >

						{{ csrf_field() }}

					<button id="btnFavorite" type="submit" class="btn btn-info" {{ $reply->isFavorited() || !Auth::check() ? 'disabled="disable"' : '' }}>

						{{ $reply->favorites()->count()}}

						<span class="glyphicon glyphicon-heart red" aria-hidden="true"></span>

					</button>

				</form>

			</div>

	    </div>

    </div>

    <div class="panel-body">

    	{{$reply->body}}

    </div>

 </div>