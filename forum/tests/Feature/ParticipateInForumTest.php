<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ParticipateInForumTest extends TestCase {

	use DatabaseMigrations;

	public function test_unauthenticated_users_may_not_add_replies() {
		$this->expectException('Illuminate\Auth\AuthenticationException');
		$this->post('threads/some-channel/1/replies', []);
	}

	public function test_an_authenticated_user_may_participate_in_forum_threads() {
		//Given we have a authenticated user
		$this->be($user = factory('App\User')->create());

		//And an existing thread
		$thread = factory('App\Thread')->create();
		$reply = factory('App\Reply')->create();

		//when the user adds a reply to the thread
		$this->post($thread->path() . '/replies', $reply->toArray());

		//Then their reply should be visible on the page
		$this->get($thread->path())
			->assertSee($reply->body);
	}

	public function test_a_reply_requires_a_body() {

		$this->withExceptionHandling()->signIn();
		$thread = create('App\Thread');
		$reply = make('App\Reply', ['body' => null]);
		$this->post($thread->path() . '/replies', $reply->toArray())
			->assertSessionHasErrors('body');

	}
}
