<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CreateThreadsTest extends TestCase {

	use DatabaseMigrations;

	public function test_guests_may_not_create_threads() {
		$this->expectException('Illuminate\Auth\AuthenticationException');
		$thread = factory('App\Thread')->make();
		$this->post('/threads', $thread->toArray());
	}

	public function test_guests_cannot_see_the_create_thread_page() {
		// $this->expectException('Illuminate\Auth\AuthenticationException');
		$this->withExceptionHandling();

		$this->get('/threads/create')
			->assertRedirect('login');

		$this->post('/threads/', [])
			->assertRedirect('login');

	}

	public function test_an_authenticate_user_can_create_new_forum_threads() {
		//Given we have a signed in user
		$this->signIn();

		//when wehit the endpoint to create a new thread
		$thread = make('App\Thread');

		//Then when we visit the thread page
		$reponse = $this->post('/threads', $thread->toArray());

		//We should see the new thread
		$this->get($reponse->headers->get('Location'))
			->assertSee($thread->title)
			->assertSee($thread->body);
	}

	public function test_a_thread_requires_a_titile() {
		$this->createThread(['title' => null])
			->assertSessionHasErrors('title');

	}

	public function test_a_thread_requires_a_body() {
		$this->createThread(['body' => null])
			->assertSessionHasErrors('body');

	}

	public function test_a_thread_requires_a_valid_channel() {
		$this->createThread(['channel_id' => null])
			->assertSessionHasErrors('channel_id');

	}

	public function createThread($overrides = []) {

		$this->withExceptionHandling()->signIn();

		$thread = make('App\Thread', $overrides);

		return $this->post('/threads', $thread->toArray());
	}
}
