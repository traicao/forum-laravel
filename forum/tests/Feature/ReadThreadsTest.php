<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ReadThreadsTest extends TestCase {

	use DatabaseMigrations;

	public function setUp() {
		parent::setUp();
		$this->thread = factory('App\Thread')->create();
		$this->reply = factory('App\Reply')->create(['thread_id' => $this->thread->id]);
	}

	public function test_a_user_can_view_all_threads() {
		$this->get('/threads')
			->assertSee($this->thread->title);
	}

	public function test_a_user_can_read_a_single_thread() {
		$this->get($this->thread->path())
			->assertSee($this->thread->title);
	}

	public function test_a_user_can_read_replies_that_are_associated_with_a_thread() {
		$this->get($this->thread->path())
			->assertSee($this->reply->body);
	}

	public function test_a_user_can_filter_threads_according_to_a_channel() {
		$channel = create('App\Channel');
		$threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
		$threadNotInChannel = create('App\Thread');
		$this->get('/threads/' . $channel->slug)
			->assertSee($threadInChannel->title)
			->assertDontSee($threadNotInChannel->title);
	}

	public function test_a_user_can_filter_threads_by_any_username() {

		$this->signIn(create('App\User', ['name' => 'trai']));

		$threadsByTrai = create('App\Thread', ['user_id' => auth()->id()]);

		$threadsNotByTrai = create('App\Thread');

		$this->get('/threads?by=trai')
			->assertSee($threadsByTrai->title)
			->assertDontSee($threadsNotByTrai->title);

	}

	// Test fail when pagination...
	/*public function test_a_user_can_filter_threads_by_popular() {

		$threadWithTwoReplies = create('App\Thread');
		create('App\Reply', ['thread_id' => $threadWithTwoReplies], 2);

		$threadWithThreeReplies = create('App\Thread');
		create('App\Reply', ['thread_id' => $threadWithThreeReplies], 3);

		$reponse = $this->getJson('threads?popular=1')->json();

		$this->assertEquals([3, 2, 1], array_column($reponse, 'replies_count'));

	}
	*/

	public function test_guests_can_not_delete_threads() {

		$this->withExceptionHandling();
		$reponse = $this->delete($this->thread->path());
		$reponse->assertRedirect('/login');
	}

	public function test_a_thread_can_be_deleted() {

		$this->signIn();
		$this->json('DELETE', $this->thread->path());
		$this->assertDatabaseMissing('threads', ['id' => $this->thread->id])
			->assertDatabaseMissing('replies', ['thread_id' => $this->thread->id]);

	}

	public function test_threads_may_only_be_deleted_by_those_who_have_permission() {

	}

}
