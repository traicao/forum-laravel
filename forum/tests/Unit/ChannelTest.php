<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ChannelTest extends TestCase {

	use DatabaseMigrations;
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function test_a_channel_consists_of_threads() {
		$channel = create('App\Channel');
		$threads = create('App\Thread', ['channel_id' => $channel->id]);
		$this->assertTrue($channel->threads->contains($threads));
	}
}
