<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ActivityTest extends TestCase {
	use DatabaseMigrations;
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function test_it_records_activity_when_a_thread_is_created() {
		$this->signIn();
		$thread = create('App\Thread');
		$this->assertDataBaseHas('activities', [
			'subject_id' => $thread->id,
			'type' => 'created_thread',
			'user_id' => auth()->id(),
			'subject_type' => 'App\Thread',
		]);

		$activity = \App\Activity::first();

		$this->assertEquals($activity->subject->id, $thread->id);

	}

	public function test_it_records_activity_when_a_reply_is_created() {
		$this->signIn();
		$reply = create('App\Reply');
		$this->assertDataBaseHas('activities', [
			'subject_id' => $reply->id,
			'type' => 'created_reply',
			'user_id' => auth()->id(),
			'subject_type' => 'App\Reply',
		]);

		$activity = \App\Activity::first();

		$this->assertEquals($activity->subject->id, $reply->id);

	}
}
