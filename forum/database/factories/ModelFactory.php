<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {

	$filepath = storage_path('app/public/images');

	if (!File::exists($filepath)) {
		File::makeDirectory($filepath);
	}
	return [
		'name' => $faker->name,
		'email' => $faker->unique()->safeEmail,
		'avatar' => $faker->image($filepath, 400, 300),
		'password' => bcrypt('secret'),
		'remember_token' => str_random(10),
	];
});

$factory->define(App\Thread::class, function ($faker) {
	return [
		'user_id' => function () {
			return factory('App\User')->create()->id;
		},
		'channel_id' => function () {
			return factory('App\Channel')->create()->id;
		},
		'title' => $faker->sentence,
		'body' => $faker->paragraph,
	];
});

$factory->define(App\Channel::class, function ($faker) {
	$name = $faker->word;
	return [
		'name' => $name,
		'slug' => $name,
	];
});

$factory->define(App\Reply::class, function ($faker) {
	return [
		'thread_id' => function () {
			return factory('App\Thread')->create()->id;
		},
		'user_id' => function () {
			return factory('App\User')->create()->id;
		},
		'body' => $faker->paragraph,
	];
});
